---
tags: ["CHTC"]
---
# Welcome to BCC

This is the documentation for using the [Biochemistry Compute Cluster](https://bcrf.biochem.wisc.edu/bcc/), is a High Throughput Computing (HTC) environment available to the University of Wisconsin-Madison Department of Biochemistry. 

The BCC utilizes [HTCondor](http://research.cs.wisc.edu/htcondor/), which is developed by the [Computer Sciences Department](https://www.cs.wisc.edu) at the UW-Madison.

All Madison researchers can also obtain a free account at the
UW [Center for High Throughput Computing](http://chtc.cs.wisc.edu/) (CHTC) which can also accomodate larger compute projects.

The [QuickStart](quickstart.md) documents should help users familiar with `bash` and `HTCondor` to get started.




